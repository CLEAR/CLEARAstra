% how to turn the input file into different number of particles when don't
% have generator

one = load('laser3.4ps_gaus_1000_0.4mm.txt');
[m,n] = size(one);
X=zeros(2*m,n);

k=5; %have k thousand particles

for i=1:k;
    M=(i-1).*m;
    X(M+1:M+m,:)=one(:,:);
end

%% fewer particles

one = load('laser3.4ps_gaus_1000_0.4mm.txt');

N=200;
D=1000-N;                               %D=1000-N
X=round(rand(D,1).*999)+1;         %N uniformly randomised numbers between 1 and 1000
X=sort(X, 'descend');               %take out higher indices first so dont exceed limit of X
for i=1:D;
    x=X(i);
    one(x,:) = [];
end

x=one(:,1);
y=one(:,2);
z=one(:,3);
hist(x)                             %check still Gaussian

save '200.dat' one -ascii
length(one)                         %check sth funny hasnt happened

