one = load('laser3.4ps_gaus_1000_0.4mm.txt'); %califes_linac.1800.001
x=one(:,1);
y=one(:,2);
z=one(:,3);
scatter(x,y, 'x')
hold on

X=x+0.0005; %in metres
Y=y+0.0005;
XYmatrix(:,1)=X;
XYmatrix(:,2)=Y;
XYmatrix(:,3:10)=one(:,3:end);
% save 'shifted_distribution.txt' XYmatrix -ascii

two = load('shifted_distribution.txt');
x2=two(:,1);
y2=two(:,2);
z2=two(:,3);
scatter(x2,y2, 'k')

%  load califes_linac.Xemit.002
%  z4=califes_linac_Xemit(:,1);
%  ex=califes_linac_Xemit(:,6);
%  plot(z4,ex);
% figure