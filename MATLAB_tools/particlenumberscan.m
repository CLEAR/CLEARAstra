%% to plot output emittances as a function of particle number
% you need to update max i (and additive j) to select different range of
% runs


clear
tmp = dir('/nfs/cs-ccr-nfs6/vol29/Linux/data/clear/ASTRAModel/astra/califes_linac.Xemit.*'); %Load all files in current directory
filesToProcess = {tmp.name};
clear tmp;

tmp = dir('/nfs/cs-ccr-nfs6/vol29/Linux/data/clear/ASTRAModel/astra/califes_linac.Yemit.*');
filesToProcess2 = {tmp.name};
clear tmp;

tmp = dir('/nfs/cs-ccr-nfs6/vol29/Linux/data/clear/ASTRAModel/astra/califes_linac.Zemit.*');
filesToProcess3 = {tmp.name};
clear tmp;

N = 17;     %max i

for i=1:N;          %scans
    j=i+23;         %to get correct run numbers
    Xemit = load(filesToProcess{j});            %should be a file with many rows corresponding to different positions
    Yemit = load(filesToProcess2{j});
    Zemit = load(filesToProcess3{j});
    xrms(i)=Xemit(length(Xemit),4);             %to access final values go to end of *emit          
    xemit(i)=Xemit(length(Xemit),6);                
    yrms(i)=Yemit(length(Yemit),4);
    yemit(i)=Yemit(length(Yemit),6);
    zrms(i)=Zemit(length(Zemit),4);
    energy(i)=Zemit(length(Zemit),3);
    clear Xemit;
    clear Yemit;
    clear Zemit;

    str=sprintf('grep .dat /nfs/cs-ccr-nfs6/vol29/Linux/data/clear/ASTRAModel/astra/califes_linac.Log.0%d',j);
    [~,foundStrings] = system(str); % search for string .dat in file califes_linac.Log.0j
    splitStrings = strsplit(foundStrings,'='); %chop it up however necessary
    lastsplit = str2double(strsplit(splitStrings{2},'.'));
    number(i) = lastsplit(1);
end
number(14)=200; %forced because of different name format

interest = xemit;
name = 'Energy/MeV';
hold on

M(:,1)=number;      %M is matrix to temporarily hold N and var of interest
M(:,2)=interest;
A=sortrows(M);     %sorts based on first column preseriving rows
N=A(:,1);
y=A(:,2);

plot(N,y,'x')
xlabel('Number of macroparticles');
ylabel(sprintf('%s', name));
set(gca, 'FontSize',14);
box on