load profil_entier_bob-THOMX.txt        %solenoid
z= profil_entier_bob_THOMX(:,1);
B= profil_entier_bob_THOMX(:,2);
plot(z,B);
figure

load TWS_LIL2.txt                       %travelling wave E-field
z2= TWS_LIL2(:,1);
E=TWS_LIL2(:,2);
plot(z2,E);
figure

load sonde_ideal_SF_100.txt             %static E-field
z3 = sonde_ideal_SF_100(:,1);
E2 = sonde_ideal_SF_100(:,2);
plot(z3,E2);
figure

load laser3.4ps_gaus_1000_0.4mm.txt     %initial distribution as sent by LAL
x= laser3_4ps_gaus_1000_0_4mm(:,1);
y= laser3_4ps_gaus_1000_0_4mm(:,2);
z5=laser3_4ps_gaus_1000_0_4mm(:,3);
px=laser3_4ps_gaus_1000_0_4mm(:,4);
py=laser3_4ps_gaus_1000_0_4mm(:,5);
pz=laser3_4ps_gaus_1000_0_4mm(:,6);
plot(pz)