%%
x=[0.2,0.2222,0.24,0.25,0.255,0.26,0.265,0.268,0.27,0.275,0.28,0.3];
y=[18.41,13.83,10.50,9.02,8.47,8.13,8.028,8.095,8.19,8.645,9.36,13.85];
plot(x,y)
xlabel('B/T');
ylabel('Transverse beam size (in horizontal) /mm')
title('Transverse beam size variation with magnetic field strength')
set(gca, 'FontSize',14);
box on

%%
tmp = dir('califes_linac.Xemit.*'); %Load all Simple files in current directory
filesToProcess = {tmp.name};
clear tmp;

tmp = dir('califes_linac.Yemit.*'); %Load all Simple files in current directory
filesToProcess2 = {tmp.name};
clear tmp;

tmp = dir('califes_linac.Zemit.*'); %Load all Simple files in current directory
filesToProcess3 = {tmp.name};
clear tmp;

for i=1:10          %scans
    j=i+10;         %to get correct run numbers
    Xemit = load(filesToProcess{j});
    Yemit = load(filesToProcess2{j});
    Zemit = load(filesToProcess3{j});
    xrms(i)=Xemit(length(Xemit),4);                 
    xemit(i)=Xemit(length(Xemit),6);                
    yrms(i)=Yemit(length(Yemit),4);
    yemit(i)=Yemit(length(Yemit),6);
    zrms(i)=Zemit(length(Zemit),4);
    energy(i)=Zemit(length(Zemit),3);
    clear Xemit;
    clear Yemit;
    clear Zemit;
end


%% to extract the Bfield strength from Log files

for i=1:10;
j=i+10;
str=sprintf('grep MAXB califes_linac.Log.0%d',j);
[~,foundStrings] = system(str); % search for string MAXB in file califes_linac.Log.021
splitStrings = strsplit(foundStrings,'=');
lastsplit = str2double(strsplit(splitStrings{3},','));
MAXB(i) = lastsplit(1);
end

XY(:,1)=MAXB;
XY(:,2)=energy;
%XY(:,3)=xemit;
A=sortrows(XY);     %sorts based on first column preseriving rows
B=A(:,1);
y=A(:,2);
%x=A(:,3);
plot(B,y);
% hold on
% plot(B,x);
xlabel('B/T');
ylabel('Kinetic Energy/MeV')
title('Output kinetic energy variation with magnetic field strength')
set(gca, 'FontSize',14);
box on

%% to run Astra from here
system('Astra califes_linac.in')